def check(newpos,l):
    if l[int(newpos[1])-1][ord(newpos[0])-97] == 'k':
        return 1
    elif l[int(newpos[1])-1][ord(newpos[0])-97] == 'K':
        return 2
    else:
        return 0
def move(pos,newpos,l):
    l[int(newpos[1])-1][ord(newpos[0])-97]=l[int(pos[1])-1][ord(pos[0])-97]
    l[int(pos[1])-1][ord(pos[0])-97] = ' '
    return l 
class Pieces:
     def __init__(self,pos,colour):
         self.pos=pos
#         self.colour=colour
#     def move(self,pos,newpos,l):
#             l[int(newpos[1])-1][ord(newpos[0])-97]=l[int(pos[1])-1][ord(pos[0])-97]
#             l[int(pos[1])-1][ord(pos[0])-97] = '.'
#             self.pos=newpos
#             return l 
class pawn:
    def __init__(self,pos,colour,l,name):
        if l[int(pos[1])-1][ord(pos[0])-97] == 'p':
            self.colour = 'black'
        else:
            self.colour = 'white'
        self.name=name
        self.pos=pos
        if self.colour == 'black':
            self.bname='p'
        else:
            self.bname='P'
    def move(self,pos,newpos,l):
            l[int(newpos[1])-1][ord(newpos[0])-97]=l[int(pos[1])-1][ord(pos[0])-97]
            l[int(pos[1])-1][ord(pos[0])-97] = ' '
            self.pos=newpos
            return l 
            
class rook:
    def __init__(self,pos,colour,l,name):
        if l[int(pos[1])-1][ord(pos[0])-97] == 'r':
            self.colour = 'black'
        else:
            self.colour = 'white'
        self.name=name
        self.pos=pos
        if self.colour == 'black':
            self.bname='r'
        else:
            self.bname='R'
    def move(self,pos,newpos,l):
            l[int(newpos[1])-1][ord(newpos[0])-97]=l[int(pos[1])-1][ord(pos[0])-97]
            l[int(pos[1])-1][ord(pos[0])-97] = ' '
            self.pos=newpos
            return l 
class knight:
    def __init__(self,pos,colour,l,name):
        if l[int(pos[1])-1][ord(pos[0])-97] == 'n':
            self.colour = 'black'
        else:
            self.colour = 'white'
        self.name=name
        self.pos=pos
        if self.colour == 'black':
            self.bname='n'
        else:
            self.bname='N'
    def move(self,pos,newpos,l):
            l[int(newpos[1])-1][ord(newpos[0])-97]=l[int(pos[1])-1][ord(pos[0])-97]
            l[int(pos[1])-1][ord(pos[0])-97] = ' '
            self.pos=newpos
            return l 
class bishop:
    def __init__(self,pos,colour,l,name):
        if l[int(pos[1])-1][ord(pos[0])-97] == 'b':
            self.colour = 'black'
        else:
            self.colour = 'white'
        self.name=name
        self.pos=pos
        if self.colour == 'black':
            self.bname='b'
        else:
            self.bname='B'
    def move(self,pos,newpos,l):
            l[int(newpos[1])-1][ord(newpos[0])-97]=l[int(pos[1])-1][ord(pos[0])-97]
            l[int(pos[1])-1][ord(pos[0])-97] = ' '
            self.pos=newpos
            return l 
class queen:
    def __init__(self,pos,colour,l,name):
        if l[int(pos[1])-1][ord(pos[0])-97] == 'q':
            self.colour = 'black'
        else:
            self.colour = 'white'
        self.name=name
        self.pos=pos
        if self.colour == 'black':
            self.bname='q'
        else:
            self.bname='Q'
    def move(self,pos,newpos,l):
            l[int(newpos[1])-1][ord(newpos[0])-97]=l[int(pos[1])-1][ord(pos[0])-97]
            l[int(pos[1])-1][ord(pos[0])-97] = ' '
            self.pos=newpos
            return l 
class king:
    def __init__(self,pos,l,name):
        if l[int(pos[1])-1][ord(pos[0])-97] == 'k':
            self.colour = 'black'
        else:
            self.colour = 'white'
        self.name=name
        self.pos=pos
        if self.colour == 'black':
            self.bname='k'
        else:
            self.bname='K'
    def move(self,pos,newpos,l):
            l[int(newpos[1])-1][ord(newpos[0])-97]=l[int(pos[1])-1][ord(pos[0])-97]
            l[int(pos[1])-1][ord(pos[0])-97] = ' '
            self.pos=newpos
            return l 