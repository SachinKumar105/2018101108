import Pieces

class Board:
    def __init__(self):
        self.board = []
        for i in range(8):
            temp = []
            for j in range(8):
                temp.append('')
            self.board.append(temp)
    def disp(self):
        n=int(1)
        strf = ''
        r=0
        c=0
        for row in self.board:
            r=r+1
            c=0
            for i in range(2):
                for obj in row:
                    if i%2 == 0: # Print Board Scaffolding
                        strf += '+---'
                    else: # Print objects
                        c=c+1
                        strf += '|'
                        #strf += obj.disp()
                        if self.board[r-1][c-1] == '':
                            strf += '   '
                        else:
                            strf += ' ' + self.board[r-1][c-1] + ' '
                if i%2 == 0:
                    strf += '+'
                else:
                    strf += '|'
                    strf += ' ' + str(n)
                    n=n+1
                strf += '\n'
        for obj in self.board[-1]:
            strf += '+---'
        strf += '+' + '\n'
        strf += str( '  a  '' b  '' c  '' d  '' e  '' f  '' g  '' h  ')
        print(strf)


# a = Board()
# a.board[0][0]='R' #Pieces.rook(chr(97)+'1',a.board,1).bname
# a.board[0][7]='R' #Pieces.rook(chr(97+7)+'1',a.board,2).bname
# a.board[0][1]='K' #Pieces.knight(chr(97+1)+'1',a.board,1).bname
# a.board[0][6]='K' #Pieces.knight(chr(97+6)+'1',a.board,2).bname
# a.board[0][2]='B' #Pieces.bishop(chr(97+2)+'1',a.board,1).bname
# a.board[0][5]='B' #Pieces.bishop(chr(97+5)+'1',a.board,2).bname
# a.board[0][3]='K' #Pieces.king(chr(97+3)+'1',a.board,1).bname
# a.board[0][4]='Q' #Pieces.queen(chr(97+4)+'1',a.board,1).bname
# for i in range(8):
#     a.board[1][i]='P' #Pieces.pawn(chr(97+i)+'2',a.board,str(i+1)).bname
# a.board[7][0]=a.board[7][7]='r'
# a.board[7][1]=a.board[7][6]='n'
# a.board[7][2]=a.board[7][5]='b'
# a.board[7][3]='k'
# a.board[7][4]='q'
# for i in range(8):
#     a.board[6][i]='p'
# print(a.board[1][2])
# print()
# a.disp()
# print()
# a.board=Pieces.move('a7','a6',a.board)
# a.disp()
# print(Pieces.pawn(chr(97+i)+'2','black',a.board,3).pos)