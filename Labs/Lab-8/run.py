import Board
import Pieces
a = Board.Board()
a.board[0][0]='R' #Pieces.rook(chr(97)+'1',a.board,1).bname
a.board[0][7]='R' #Pieces.rook(chr(97+7)+'1',a.board,2).bname
a.board[0][1]='N' #Pieces.knight(chr(97+1)+'1',a.board,1).bname
a.board[0][6]='N' #Pieces.knight(chr(97+6)+'1',a.board,2).bname
a.board[0][2]='B' #Pieces.bishop(chr(97+2)+'1',a.board,1).bname
a.board[0][5]='B' #Pieces.bishop(chr(97+5)+'1',a.board,2).bname
a.board[0][3]='Q' #Pieces.king(chr(97+3)+'1',a.board,1).bname
a.board[0][4]='K' #Pieces.queen(chr(97+4)+'1',a.board,1).bname
for i in range(8):
    a.board[1][i]='P' #Pieces.pawn(chr(97+i)+'2',a.board,str(i+1)).bname
a.board[7][0]=a.board[7][7]='r'
a.board[7][1]=a.board[7][6]='n'
a.board[7][2]=a.board[7][5]='b'
a.board[7][3]='q'
a.board[7][4]='k'
for i in range(8):
    a.board[6][i]='p'
a.disp()
print("Player1 are small letters and Player2 are capital letters")
while True:
    #print("Player1's move")
    # x=input("Old_position: ")
    # y=input("New_position: ")
    z = input("Player1's move: ")
    # x,y = input("Player1's move: ").split()
    if z == '0-0':
        a.board=Pieces.move('e8','g8',a.board)
        a.board=Pieces.move('h8','f8',a.board)
        a.disp()
    elif z == '0-0-0':
        a.board=Pieces.move('e8','c8',a.board)
        a.board=Pieces.move('a8','d8',a.board)
        a.disp()
    else:
        x,y = z.split()    
        if Pieces.check(y,a.board) == 1:
            a.board=Pieces.move(x,y,a.board)
            a.disp()
            print(" Player2 Wins ")
            break
        elif Pieces.check(y,a.board) == 2:
            a.board=Pieces.move(x,y,a.board)
            a.disp()
            print(" Player1 Wins ")
            break
        else:
            a.board=Pieces.move(x,y,a.board)
            a.disp()
    # print("Player2's move")
    # x=input("Old_position: ")
    # y=input("New_position: ")
    w = input("Player2's move: ")
    if w == '0-0':
        a.board=Pieces.move('e1','g1',a.board)
        a.board=Pieces.move('h1','f1',a.board)
        a.disp()
    elif w == '0-0-0':
        a.board=Pieces.move('e1','c1',a.board)
        a.board=Pieces.move('a1','d1',a.board)
        a.disp()
    else:
        x,y = w.split()
        if Pieces.check(y,a.board) == 1:
            a.board=Pieces.move(x,y,a.board)
            a.disp()
            print(" Player2 Wins ")
            break
        elif Pieces.check(y,a.board) == 2:
            a.board=Pieces.move(x,y,a.board)
            a.disp()
            print(" Player1 Wins ")
            break
        else:
            a.board=Pieces.move(x,y,a.board)
            a.disp()