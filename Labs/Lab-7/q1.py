def isLeapYear(year):
    year=int(year)
    if year%100==0:
        if year % 400 == 0:
            return 1
        else:
            return 0
    elif year % 4 == 0:
        return 1
    else:
        return 0
year=input()
q=isLeapYear(year)
print(q)