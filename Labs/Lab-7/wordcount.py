import re
import string
frequency = {}
document_text = open('sys.argv[1]', 'r')
text_string = document_text.read().lower()
match_pattern = re.findall(r'\b[a-z]{3,15}\b', text_string)
 
for word in match_pattern:
    count = frequency.get(word,0)
    frequency[word] = count + 1
     
frequency_list = frequency.keys()
file=open('sys.argv[2]','w')
for words in frequency_list:
    file.write(words,":", frequency[words])
    file.write("\n")