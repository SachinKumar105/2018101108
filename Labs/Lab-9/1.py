from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask import request
from flask import jsonify

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True , autoincrement = True )
    rollnumber = db.Column(db.Integer, unique=True)
    sname = db.Column(db.String(120), unique=True)
    email = db.Column(db.String(120), unique=True)
    def __init__(self, rollnumber, sname, email):
        self.rollnumber = rollnumber
        self.sname = sname
        self.email = email

    def __repr__(self):
        return '<User %r>' % self.sname

@app.route("/students/create", methods=["POST"])
def userAdd():
    roll=request.form['rollnumber']
    sname= request.form['sname']
    email= request.form['email']
    db.create_all()
    new_person=User(roll, sname, email)
    db.session.add(new_person)
    db.session.commit()
    temp ={}
    temp['status']=(type(new_person)==User)
    return jsonify(temp)

@app.route("/students/")
def userFetch():
    db.create_all()
    allUsers=User.query.all()
    strf = ''
    for user in allUsers:
        strf += str(user.rollnumber) + " " + user.sname + " " + user.email + "\n"
    return strf

@app.route("/students/delete", methods=["POST"])
def userDelete():
    roll=request.form['rollnumber']
    db.create_all()
    person=User.query.filter_by(rollnumber=roll).first()
    db.session.delete(person)
    db.session.commit()
    return "true"

@app.route("/courses/<code>", methods=["POST"])
def coursesUpdate(code):
    course = Course.query.filter_by(code=code).first()
    new_name = request.form['name']
    db.create_all()
    course.name = new_name
    db.session.commit()
    strr = str(code) + " " + new_name + "\n"
    temp = {}
    temp['status'] = (type(course) == Course)
    return strr
    
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)